#pragma once

#include <string>
using namespace std;

class Group;

class Student
{
	private:

		int id;			//Id;
		string fio;		//�������, ��� � ��������;
		Group* gr;		//��������� �� ���� ������;
		int* marks;		//������ ������;
		int num;		//���������� ������;

	public:

		Student(int id, string fio);

		Student(Student& st);

		~Student();

		//�������;
		void setGroup(Group* grp);

		//�������;
		Group* getGroup();
		string getFIO();
		int getID();
		int getNumMarks();
		int* getMarks();
		friend ostream& operator<<(ostream& os, const Student& student);

		//���������� ������;
		void addMark(int n);

		//��������� ������� ������;
		double getAvMark();

		//��������� ����������� ������;
		int getMinMarks();

		//��������� ���������� ������������ ������;
		int howMarks(int mark);
};

