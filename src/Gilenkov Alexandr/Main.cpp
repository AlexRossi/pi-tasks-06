#include <iostream>
#include <clocale>
#include "Student.h"
#include "Group.h"
#include "Dekanat.h"
using namespace std;

void DekanatDemonstration(Dekanat* dof); // ���� ������ ��������
void AddStudentToNewGroupDemonstration(Student* student, Group* group, Dekanat* dof); // ���� ���������� ������ �������� � ����� ������
void ChangeGroupDemonstration(Dekanat* dof); // ���� �������� ������ �������� �� ����� ������ � ������������
void Check(Dekanat* dof); // �������� �� ��������� ���������

int main()
{
	setlocale(LC_CTYPE, "rus"); // ����� ��������� �� �������
	Dekanat* dof = new Dekanat();
	Group* group = new Group("381600"); // �������� ����� ������
	Student* student = new Student(0, "������ ���� ��������", nullptr); // �������� ������ ��������
	DekanatDemonstration(dof);
	AddStudentToNewGroupDemonstration(student, group, dof);
	ChangeGroupDemonstration(dof);
	Check(dof);
	return 0;
}

void DekanatDemonstration(Dekanat* dof)
{
	dof->LoadGroups(); // �������� ����� �� TXT �����
	dof->LoadStudents(); // �������� ��������� �� TXT �����
	dof->AddRandomMarks(); // ���������� ��������� ������ ���������
	dof->RandomGroupsHeads(); // ��������� ���������� �������
	dof->StudentsAverageMarks(); // ������� ������ ���������
	dof->GroupsAverageMarks(); // ������� ������ � �������
	dof->SendingDown(); // ���������� ���������
	dof->ShowHighAchievers(); // ������ �������� �����
	dof->ShowAllGroups(); // ����� ���� � �������
	dof->ShowAllStudents();// ����� ���� � ���������
	dof->SaveNewFileGroupTXT(); // ���������� ����� ������ � ������� � ����
	dof->SaveNewFileStudentTXT(); // ���������� ����� ������ � ��������� � ����
}

void AddStudentToNewGroupDemonstration(Student* student, Group* group, Dekanat* dof)
{
	dof->AddGroup(group); // ���������� ����� ������ � ������������ ������ �����
	dof->AddStudent(student); // ���������� ������ �������� � ������������ ������ ���������
	dof->AddRandomMarks(); // ���������� ������ ������ �������� � +10 ������ ���������
	dof->FindGroup("381600")->AddStudent(dof->FindStudent(0)); // ���������� ������ �������� � ����� ������
}

void ChangeGroupDemonstration(Dekanat* dof)
{
	dof->FindGroup("381600")->ShowGroupInfo(cout); // ����� ���� � ����� ������
	dof->ChangeGroup(dof->FindStudent(0), dof->FindGroup("381303")); // ������� ������ �������� � ������������ ������
}

void Check(Dekanat* dof)
{
	dof->FindGroup("381600")->ShowGroupInfo(cout); // ����� ���� � ����� ������
	dof->FindGroup("381303")->ShowGroupInfo(cout); // ����� ���� � ������������ ������
}